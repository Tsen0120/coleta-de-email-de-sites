import re
import requests
import requests.exceptions
from urllib.parse import urlsplit
from collections import deque
from bs4 import BeautifulSoup

# Coloque o site que você quer coletar os emails.
starting_url = 'coloque o seu site aqui'

# Uma fila de URLs a serem rastreados
unprocessed_urls = deque([starting_url])

# Conjunto de URLs já rastreados para email
processed_urls = set()

# Um conjunto de emails buscados
emails = set()

# Urls de processo, um por um, da fila não processada_url até a fila ficar vazia
while len(unprocessed_urls):

    # Mover o próximo URL da fila para o conjunto de URLs processados
    url = unprocessed_urls.popleft()
    processed_urls.add(url)

    # Extrair o URL base para resolver links relativos
    parts = urlsplit(url)
    base_url = "{0.scheme}://{0.netloc}".format(parts)
    path = url[:url.rfind('/')+1] if '/' in parts.path else url

    # Obter conteúdo do URL
    print("Crawling URL %s" % url)
    try:
        response = requests.get(url)
    except (requests.exceptions.MissingSchema, requests.exceptions.ConnectionError):
        # ignore pages with errors and continue with next url
        continue

    # Extrair todos os endereços de email e adicioná-los ao conjunto resultante
    # Você pode editar a expressão regular conforme sua exigência
    new_emails = set(re.findall(r"[a-z0-9\.\-+_]+@[a-z0-9\.\-+_]+\.[a-z]+", response.text, re.I))
    emails.update(new_emails)
    print(emails)